import Link from "next/link";

import styles from './redirectlink.module.css'

export function RedirectLink({ children, href }) {
  return <Link href={href} className={styles.redirectLink}>{children}</Link>;
}
