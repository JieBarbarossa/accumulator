import styles from "./sharebutton.module.css";

export default function ShareButton({ children, shareText }) {
  return (
    <button
      onClick={() => handleClickShareButton(shareText)}
      className={styles.shareButton}
    >
      {children}
    </button>
  );
}

async function handleClickShareButton(shareText) {
  navigator.clipboard.writeText(shareText);
  alert("Copied!");
}
