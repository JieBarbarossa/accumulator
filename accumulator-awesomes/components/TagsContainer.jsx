import styles from "./tagscontainer.module.css";

export default function TagsContainer({ children, tags, handleClick }) {
  return (
    <ul className={styles.TagsContainer}>
      {tags.map((tag) => (
        <li key={tag} onClick={() => handleClick(tag)}>
          <span>{tag}</span>
          {children}
        </li>
      ))}
    </ul>
  );
}
