import styles from './searchbar.module.css'

export default function SearchBar({ filterText, onFilterTextChange }) {
  return (
    <input
      type="text"
      placeholder="Search..."
      value={filterText}
      onChange={(e) => onFilterTextChange(e.target.value)}
      className={styles.searchBar}
    />
  );
}
