function groupBy(objectArray, property) {
  return objectArray.reduce(function (acc, obj) {
    // key is a string or number
    let key = obj[property];
    if (!acc[key]) {
      acc[key] = [];
    }
    acc[key].push(obj);
    return acc;
  }, {});
}

function groupByNested(objectArray, property) {
  return objectArray.reduce(function (acc, obj) {
    // keys is an array
    let keys = obj[property];
    if (keys) {
      keys.forEach((key) => {
        if (!acc[key]) {
          acc[key] = [];
        }
        acc[key].push(obj);
      });
    }
    return acc;
  }, {});
}

export { groupBy, groupByNested };
