"server-only";

import { supabase } from "../utils/initSupabase";

import { RedirectLink } from "../components/RedirectLink";
import AwesomeApp from "./awesomes";

export default async function App() {
  // Caching data with Next.js 13 and Supabase
  // https://github.com/supabase/supabase/tree/master/examples/caching/with-nextjs-13
  let { data, err } = await supabase
    .from("awesomes")
    .select(`name, url, description, tags, update_date`)
    .range(0, 20);

  if (err) {
    alert(err);
    console.log(err);
  } else {
    return (
      <>
        <AwesomeApp data={data} />
        <RedirectLink href="/new">New</RedirectLink>
      </>
    );
  }

  // const DATA = [
  //   {
  //     name: "A1",
  //     url: "https://unicode-org.github.io/unicodetools/help/",
  //     description:
  //       "The Unicode Utilities provide a number of different utilities that use and demonstrate features of the Unicode encoding and locale data.",
  //     tags: ["tag2"],
  //     update_date: "2022-11-12",
  //   },
  //   {
  //     name: "A2",
  //     url: "https://unicode-org.github.io/unicodetools/help/",
  //     description:
  //       "The Unicode Utilities provide a number of different utilities that use and demonstrate features of the Unicode encoding and locale data.",
  //     tags: ["tag1", "tag2", "tag3"],
  //     update_date: "2022-11-14",
  //   },
  //   {
  //     name: "A3",
  //     url: "https://unicode-org.github.io/unicodetools/help/",
  //     description:
  //       "The Unicode Utilities provide a number of different utilities that use and demonstrate features of the Unicode encoding and locale data.",
  //     tags: ["tag1"],
  //     update_date: "2022-11-11",
  //   },
  //   {
  //     name: "A4",
  //     url: "https://unicode-org.github.io/unicodetools/help/",
  //     description:
  //       "The Unicode Utilities provide a number of different utilities that use and demonstrate features of the Unicode encoding and locale data.",
  //     tags: ["tag1"],
  //     update_date: "2022-11-11",
  //   },
  // ];
  
  // return (
  //   <>
  //     <AwesomeApp data={DATA} />
  //     <RedirectLink href="/new">New</RedirectLink>
  //   </>
  // );
}
