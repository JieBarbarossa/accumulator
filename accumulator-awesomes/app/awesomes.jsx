"use client";

import { useState } from "react";

import ShareButton from "../components/ShareButtton";
import SearchBar from "../components/SearchBar";
import TagsContainer from "../components/TagsContainer";

import { groupBy, groupByNested } from "../utils/utils";

import styles from "./awesomes.module.css";

function AwesomeRow({ awesome, handleFilterText }) {
  return (
    <div className={styles.awesomeRow}>
      <h2>
        <a href={awesome.url}>{awesome.name}</a>
      </h2>
      <ShareButton text={awesome.url}>Share</ShareButton>
      <p>{awesome.description}</p>
      <TagsContainer tags={awesome.tags} handleClick={handleFilterText} />
    </div>
  );
}

function AwesomeDisclosure({
  elements,
  summary,
  filterText,
  handleFilterText,
}) {
  let rows = [];

  function handleNameTagSearch(obj, filterText) {
    let fulltext = "";
    fulltext += obj.name + obj.tags;
    if (fulltext.toLowerCase().indexOf(filterText.toLowerCase()) === -1) {
      return false;
    }
    return true;
  }

  if (elements) {
    elements.forEach((element) => {
      if (handleNameTagSearch(element, filterText)) {
        rows.push(
          <AwesomeRow
            awesome={element}
            key={element.name}
            handleFilterText={handleFilterText}
          />
        );
      }
      return;
    });
  }

  return (
    <details className={styles.awesomeDisclosure} open>
      <summary>{summary}</summary>
      <ul>{rows}</ul>
    </details>
  );
}

function AwesomeColumn({ groupedData, filterText, handleFilterText }) {
  let disclosures = [];

  if (groupedData) {
    for (const key in groupedData) {
      if (Object.hasOwnProperty.call(groupedData, key)) {
        const elements = groupedData[key];
        disclosures.push(
          <AwesomeDisclosure
            elements={elements}
            summary={key}
            filterText={filterText}
            handleFilterText={handleFilterText}
            key={key}
          />
        );
      }
    }
  }

  return <div className={styles.awesomeColumn}>{disclosures}</div>;
}

export default function AwesomeApp({ data }) {
  const [filterText, setFilterText] = useState("");

  const awesomeGroupByDate = groupBy(data, "update_date");
  const awesomeGroupByTag = groupByNested(data, "tags");

  return (
    <div className={styles.container}>
      <SearchBar filterText={filterText} onFilterTextChange={setFilterText} />
      <main className={styles.main}>
        <AwesomeColumn
          groupedData={awesomeGroupByDate}
          filterText={filterText}
          handleFilterText={setFilterText}
        />
        <AwesomeColumn
          groupedData={awesomeGroupByTag}
          filterText={filterText}
          handleFilterText={setFilterText}
        />
      </main>
    </div>
  );
}
