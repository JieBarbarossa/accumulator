"use client";

import { useState } from "react";
import { supabase } from "../../utils/initSupabase";

import { RedirectLink } from "../../components/RedirectLink";
import TagsContainer from "../../components/TagsContainer";

import styles from "./page.module.css";

function Form() {
  const [record, setRecord] = useState({
    name: "",
    url: "",
    description: "",
    tags: [],
  });

  const [error, setError] = useState(null);

  async function handleSubmit(e) {
    e.preventDefault();

    let { data, err } = await supabase
      .from("awesomes")
      .insert({
        ...record,
      })
      .single();

    if (err) {
      setError(err.message);
      alert(error);
    } else {
      alert("Done!");
      setRecord({
        name: "",
        url: "",
        description: "",
        tags: [],
      });
    }
  }

  function handleChange(e) {
    setRecord({
      ...record,
      [e.target.name]: e.target.value.trim(),
    });
  }

  function handleAddTag(tag) {
    const newTags = [...record.tags, tag.trim()];
    setRecord({
      ...record,
      tags: newTags,
    });
  }

  function handleDeleteTag(tag) {
    setRecord({
      ...record,
      tags: record.tags.filter((t) => t !== tag),
    });
  }

  return (
    <form onSubmit={handleSubmit} className={styles.form}>
      <input
        type="text"
        placeholder="Name"
        tabIndex="1"
        required
        autoFocus
        name="name"
        value={record.name}
        onChange={handleChange}
      />
      <input
        type="url"
        placeholder="https://example.com"
        pattern="https://.*"
        tabIndex="2"
        required
        name="url"
        value={record.url}
        onChange={handleChange}
      />
      <textarea
        placeholder="Description"
        tabIndex="3"
        required
        name="description"
        value={record.description}
        onChange={handleChange}
      />
      <TagInput
        tags={record.tags}
        onAddTag={handleAddTag}
        onDeleteTag={handleDeleteTag}
      />
      <button type="submit">Submit</button>
    </form>
  );
}

function TagInput({ tags, onAddTag, onDeleteTag }) {
  function handleKeyUp(e) {
    if (e.key === " ") {
      onAddTag(e.target.value);
      e.target.value = "";
    }
  }

  return (
    <div className={styles.tagsInput}>
      <input
        className={styles.tagInput}
        type="text"
        placeholder="Press space to add tags"
        onKeyUp={(e) => handleKeyUp(e)}
      />
      <TagsContainer tags={tags} handleClick={onDeleteTag}>
        <span className={styles.tagRemove}>
          x
        </span>
      </TagsContainer>
    </div>
  );
}

export default function NewAwesomeApp() {
  return (
    <div className={styles.container}>
      <Form />
      <RedirectLink href="/">Home</RedirectLink>
    </div>
  );
}
